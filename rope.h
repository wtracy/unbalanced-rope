#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define SIZE 5

struct RopeNode {
  struct RopeNode* next;
  char data[SIZE];
};

struct Rope {
  struct RopeNode* data;
};

struct Rope* makeRope() {
  struct Rope* rope = malloc(sizeof(struct Rope));
  rope->data = NULL;
  return rope;
}

struct RopeNode* makeNode(char* string) {
  struct RopeNode* head = NULL;
  struct RopeNode* tail = NULL;
  struct RopeNode* newNode = NULL;
  long int remaining = strlen(string); // must use a signed type

  while (remaining > 0) {
    newNode = calloc(sizeof(struct RopeNode), 1);
    strncpy(newNode->data, string, SIZE - 1);
    remaining -= SIZE - 1;
    string += SIZE - 1;

    if (head == NULL) {
      head = newNode;
    } else {
      tail->next = newNode;
    }
    tail = newNode;
  }

	return head;
}

void appendNode(struct RopeNode* target, struct RopeNode* tail) {
  while (target->next != NULL)
    target = target->next;
  target->next = tail;
}

void appendToNode(struct RopeNode* node, char* string) {
    size_t length = strlen(node->data);
    size_t remaining = SIZE - length;

    if (remaining <= strlen(string)) {
      struct RopeNode* next = node->next;
      node->next = makeNode(string);
      appendNode(node, next);
    } else {
      strncpy((node->data + length), string, remaining);
    }
}

void append(struct Rope* rope, char* string) {
  struct RopeNode* node = rope->data;
  if (node == NULL) {
    rope->data = makeNode(string);
  } else {
    while (node->next != NULL) {
      node = node->next;
    }
    appendToNode(node, string);
  }
}

void prepend(struct Rope* rope, char* string) {
  struct RopeNode* newNode = makeNode(string);
  appendNode(newNode, rope->data);
  rope->data = newNode;
}

size_t length(struct Rope* rope) {
  struct RopeNode* node = rope->data;
  size_t size = 0;

  while (node != NULL) {
    size += strlen(node->data);
    node = node->next;
  }

  return size;
}

size_t nodeCount(struct Rope* rope) {
  struct RopeNode* node = rope->data;
  size_t count = 0;

  while (node != NULL) {
    ++count;
    node = node->next;
  }

  return count;
}

void freeRopeNode(struct RopeNode* node) {
  while (node != NULL) {
    struct RopeNode* next = node->next;
    free(node);
    node = next;
  }
}

void freeRope(struct Rope* rope) {
  if (rope->data != NULL)
    freeRopeNode(rope->data);
  free(rope);
}

/* 
Returns the node containing the provided offset, and writes the relative offset
within the node's data to the address of the original offset.

If the offset is past the end of the rope, returns the last node in the list,
and sets offset to reference the end of the last node's data.
*/
struct RopeNode* findNodeFor(struct RopeNode* node, size_t* offset) {
  size_t dataSize = strlen(node->data);

  while (dataSize < *offset) {
    if (node->next == NULL) {
      *offset = dataSize;
      break;
    }
    *offset -= dataSize;
    node = node->next;
    dataSize = strlen(node->data);
  }

  return node;
}

void getString(struct Rope* rope, char* target, size_t offset, size_t length) {
  struct RopeNode* node = rope->data;
  char* writeAddress;
  char* stopAddress = target + length - 1;
  size_t dataSize = strlen(node->data);;

  node = findNodeFor(node, &offset);

  strncpy(target, node->data + offset, length);
  writeAddress = target + dataSize;

  while (writeAddress < stopAddress) {
    node = node->next;
    if (node == NULL) {
      *writeAddress = '\0';
      break;
    }
    dataSize = strlen(node->data);
    strncpy(writeAddress, node->data, stopAddress - writeAddress);
    writeAddress += dataSize;
  }
  *stopAddress = '\0';
}

void insertAfter(struct RopeNode* parent, struct RopeNode* target) {
  appendNode(target, parent->next);
  parent->next = target;
}

void insert(struct Rope* rope, size_t offset, char* string){
  struct RopeNode* node = rope->data;
  struct RopeNode* newNode = node;

  if (node == NULL) {
    rope->data = makeNode(string);
    return;
  }

  node = findNodeFor(node, &offset);
  if (offset < strlen(node->data)) {
    /* Split existing node to make room for new data */
    newNode = makeNode(node->data + offset);
    insertAfter(node, newNode);
    node->data[offset] = '\0';
  }

  appendToNode(node, string);
}

void delete(struct Rope* rope, size_t offset, long int length){
  struct RopeNode* node = rope->data;

  node = findNodeFor(node, &offset);
  while (length > 0) {
    size_t fromCutToEnd = strlen(node->data + offset);
    if (length > fromCutToEnd) {
      /* can just truncate node */
      node->data[offset] = '\0';
      if (offset == 0) {
        /* TODO: could remove node entirely */
      }
      length -= fromCutToEnd;
      node = node->next;
    } else {
      /* need to preserve data in node after the part being cut */
      memmove(
        node->data + offset,
        node->data + offset + length,
        fromCutToEnd - length + 1);
      length = 0;
    }
    offset = 0;
  }
}

/* 
Reads data from the provided stream.

Assumes input is plaintext with no 0 bytes.
*/
void readRope(struct Rope* rope, FILE* input) {
  ssize_t amount;
  struct RopeNode* previous = rope->data;
  do {
    struct RopeNode* node = calloc(sizeof(struct RopeNode), 1);
    amount = fread(node->data, 1, SIZE - 1, input);
    if (previous == NULL)
      rope->data = node;
    else
      appendNode(previous, node);
    previous = node;
  } while (amount > 0);
}

/* Writes the rope's contents to the provided output stream. */
void writeRope(struct Rope* rope, FILE* output) {
  struct RopeNode* node = rope->data;

  while (node != NULL) {
    fwrite(node->data, 1, strlen(node->data), output);
    node = node->next;
  }
}
