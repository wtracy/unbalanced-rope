#include <stdio.h>

#include "rope.h"
#include "tinytest.h"

void testEmptyRope() {
  struct Rope* rope = makeRope();
  ASSERT_EQUALS(length(rope), 0);
  freeRope(rope);
}

void testEmptyAppend() {
  struct Rope* rope = makeRope();
  //char target[10];
  append(rope, "");
  append(rope, "");
  append(rope, "");
  ASSERT_EQUALS(length(rope), 0);
  /*getString(rope, target, 0, 0);
  ASSERT_STRING_EQUALS(target, "");*/
  freeRope(rope);
}

void testSmallRope() {
  struct Rope* rope = makeRope();
  size_t len;
  char target[10];
  append(rope, "hi");
  len = length(rope);
  ASSERT_EQUALS(2, len);
  getString(rope, target, 0, 3);
  ASSERT_STRING_EQUALS(target, "hi");
  freeRope(rope);
}

void testLengthEightRope() {
  struct Rope* rope = makeRope();
  char target[10];
  append(rope, "hihihihi");
  ASSERT_EQUALS(length(rope), 8);
  getString(rope, target, 0, 9);
  ASSERT_STRING_EQUALS(target, "hihihihi");
  ASSERT("Multiple nodes allocated", nodeCount(rope) > 1);
  freeRope(rope);
}

void testDoubleAppend() {
  struct Rope* rope = makeRope();
  char target[10];
  append(rope, "hi");
  append(rope, "hi");
  append(rope, "hi");
  ASSERT_EQUALS(length(rope), 6);
  getString(rope, target, 0, 10);
  ASSERT_STRING_EQUALS(target, "hihihi");
  ASSERT("Multiple nodes allocated", nodeCount(rope) == 2);
  freeRope(rope);
}

void testLongDoubleAppend() {
  struct Rope* rope = makeRope();
  char target[200];
  append(rope, "According to the obituary notices, a mean and unimportant person never dies.");
  append(rope, "According to the obituary notices, a mean and unimportant person never dies.");
  getString(rope, target, 0, 200);
  ASSERT_EQUALS(length(rope), 152);
  ASSERT_STRING_EQUALS(target, "According to the obituary notices, a mean and unimportant person never dies.According to the obituary notices, a mean and unimportant person never dies.");
  freeRope(rope);
}

void testPrepend() {
  struct Rope* rope = makeRope();
  char target[10];
  prepend(rope, "hi");
  prepend(rope, "prefix");
  ASSERT_EQUALS(length(rope), 8);
  getString(rope, target, 0, 9);
  ASSERT_STRING_EQUALS(target, "prefixhi");
  ASSERT("Multiple nodes allocated", nodeCount(rope) > 1);
  freeRope(rope);
}

void testInsert() {
  struct Rope* rope = makeRope();
  char target[21];

  append(rope, "==");
  insert(rope, 1, "first");

  getString(rope, target, 0, 8);
  ASSERT_STRING_EQUALS("=first=", target);

  insert(rope, 0, "st");

  getString(rope, target, 0, 10);
  ASSERT_STRING_EQUALS("st=first=", target);

  insert(rope, 9, "end");

  getString(rope, target, 0, 13);
  ASSERT_STRING_EQUALS("st=first=end", target);

  freeRope(rope);
}

void testDeleteNearFront() {
  struct Rope* rope = makeRope();
  char target[21];

  append(rope, "startohnoend");
  delete(rope, 1, 4);

  getString(rope, target, 0, 9);
  ASSERT_STRING_EQUALS("sohnoend", target);

  freeRope(rope);
}

void testDelete() {
  struct Rope* rope = makeRope();
  char target[21];

  append(rope, "startohnoend");
  delete(rope, 5, 4);

  getString(rope, target, 0, 9);
  ASSERT_STRING_EQUALS("startend", target);

  freeRope(rope);
}

void testReadWrite() {
  struct Rope* rope = makeRope();
  char target[77];

  readRope(rope, fopen("test.txt", "r"));

  getString(rope, target, 0, 77);
  ASSERT_STRING_EQUALS("According to the obituary notices, a mean and unimportant person never dies.", target);

  writeRope(rope, fopen("out.txt", "w"));

  freeRope(rope);
}

void testGetPastEnd() {
  struct Rope* rope = makeRope();
  char target[20];
  append(rope, "hi");

  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "hi");

  freeRope(rope);
}

void testGetStartingPastEnd() {
  struct Rope* rope = makeRope();
  char target[20];
  append(rope, "hi");

  getString(rope, target, 3, 20);
  ASSERT_STRING_EQUALS(target, "");

  freeRope(rope);
}

void testInsertEmptyRope() {
  struct Rope* rope = makeRope();
  char target[20];

  insert(rope, 10, "hi");

  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "hi");

  insert(rope, 10, "hi");

  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "hihi");

  freeRope(rope);
}

void stressTestInsert() {
  struct Rope* rope;
  char target[20];

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 0, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "baaaaaaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 1, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "abaaaaaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 2, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aabaaaaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 3, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aaabaaaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 4, "b");
  getString(rope, target, 0, 20);
  printf("%s\n", target);
  ASSERT_STRING_EQUALS(target, "aaaabaaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 5, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aaaaabaaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 6, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aaaaaabaa");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 7, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aaaaaaaba");
  freeRope(rope);

  rope = makeRope();
  append(rope, "aaaaaaaa");
  insert(rope, 8, "b");
  getString(rope, target, 0, 20);
  ASSERT_STRING_EQUALS(target, "aaaaaaaab");
  freeRope(rope);
}

int main() {
  RUN(testEmptyRope);
  RUN(testEmptyAppend);
  RUN(testSmallRope);
  RUN(testLengthEightRope);
  RUN(testDoubleAppend);
  RUN(testLongDoubleAppend);
  RUN(testPrepend);
  RUN(testInsert);
  RUN(testDeleteNearFront);
  RUN(testDelete);
  RUN(testReadWrite);
  RUN(testGetPastEnd);
  RUN(testGetStartingPastEnd);
  RUN(testInsertEmptyRope);
  RUN(stressTestInsert);
  return TEST_REPORT();
}
